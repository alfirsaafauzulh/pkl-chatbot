provider "aws"{
    region = "us-east-1"
    shared_credentials_file = "~/.aws/credentials"
}
//Membuat instance
//karena deploy ke default vpc jadi pake .name di secgroups
resource "aws_instance" "chatbot" {
  ami = "ami-0747bdcabd34c712a"
  instance_type = "t2.medium"
  key_name = "id_rsa"
  security_groups = [aws_security_group.SG_chatbot1.name]
  availability_zone = "us-east-1a"
  
  root_block_device {
    volume_size = 16
    volume_type = "gp2"
    delete_on_termination = true
  }
  
  tags = {
    Name = "chatbot"
  }
}

//Membuat security group dengan rule agar bisa melakukan SSH
resource "aws_security_group" "SG_chatbot1" {
  name = "chatbot1"

 ingress = [ {
    cidr_blocks = [ "0.0.0.0/0" ]
    description = null
    from_port = 22
    ipv6_cidr_blocks = null
    prefix_list_ids =  null
    protocol = "tcp"
    security_groups = null
    self = false
    to_port = 22
  }, 
  {
    cidr_blocks = [ "0.0.0.0/0" ]
    description = null
    from_port = 80
    ipv6_cidr_blocks = null
    prefix_list_ids =  null
    protocol = "tcp"
    security_groups = null
    self = false
    to_port = 80
  },
  {
    cidr_blocks = [ "0.0.0.0/0" ]
    description = null
    from_port = 443
    ipv6_cidr_blocks = null
    prefix_list_ids =  null
    protocol = "tcp"
    security_groups = null
    self = false
    to_port = 443
  }
  ] 
  egress = [ {
    cidr_blocks = [ "0.0.0.0/0" ]
    description = null
    from_port = 0
    ipv6_cidr_blocks = null
    prefix_list_ids = null
    protocol = "-1"
    security_groups = null
    self = false
    to_port = 0
  } ]
}

#Membuat elastic IP untuk chatbot
resource "aws_eip" "chatbot_ip" {
  depends_on = [
    aws_instance.chatbot
  ]
  instance = "${aws_instance.chatbot.id}"
}

//Mencetak alamat IP instance amazon linux
output "public_ip" {
  value = aws_eip.chatbot_ip.public_ip
}
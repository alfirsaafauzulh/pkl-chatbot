import sys
import os
sys.path.append(os.path.abspath("/code"))

import pytest
from program import create_app

# Create app for testing
@pytest.fixture
def client():
    app = create_app({'TESTING': True})
    with app.test_client() as client:
        yield client

def test_home(client):
    flask_app = create_app()

    # Test the '/' route 
    with flask_app.test_client() as test_client:
        response = test_client.get('/')
        assert response.status_code == 200

# Test the chatbot response
def test_bot(client):
    flask_app = create_app()
    with flask_app.test_client() as test_client:
#test
        test_client.get('/')
        response = test_client.get('/get?msg=do you like mozambique ?')
        assert b'its a great game <end> ' in response.data 